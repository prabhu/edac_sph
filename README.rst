This repository contains the code for the EDAC-SPH method.

It also contains the manuscript that was submitted in the ``manuscript``
directory. This manuscript is now accepted and available here:

https://doi.org/10.1016/j.compfluid.2018.11.023

The manuscript directory does not contain the figures. However, these figures
can be reproduced if needed.


Requirements
-------------

Install PySPH_, currently this requires the 1.0a6 release.

The other packages required are automan_, matplotlib_, psutil_ and execnet_.

.. _PySPH: http://pysph.bitbucket.io
.. _matplotlib: http://matplotlib.org
.. _psutil: https://pypi.python.org/pypi/psutil
.. _execnet: http://codespeak.net/execnet/
.. _automan: http://github.com/pypr/automan

Installation
------------

0. Setup a virtualenv or use a reasonable Python distribution where you can
   install packages.

1. Clone this repository::

     $ git clone https://gitlab.com/prabhu/edac_sph.git

2. Run the following::

     $ cd edac_sph
     $ pip install -r requirements.txt

You should be all set with the installation if everything worked correctly.


Generating the results
-----------------------

The simulations are automated with the automan_ automation framework. This
will perform all the required simulations (this can take a few days) and also
generate all the plots for the manuscript.

To use the automation code, do the following::

    $ python automate.py
    # or
    $ ./automate.py

This will run all the problems and will take a few days to complete on a
powerful machine.  By default the simulation outputs are in the ``outputs``
directory and the final plots for the paper are in ``manuscript/figures``.

If you wish to run only a few problems of interest you can do the following::

    $ ./automate.py Cavity EllipticalDrop

You could also use::

    $ ./automate.py cavity ellipticaldrop

The list of problems is case insensitive. For all the possible options please
try this::

    $ ./automate.py --help

If you wish to run just one particular simulation, try something like this::

   $ ./automate.py --match tvf_bql  TaylorGreen

If a simulation has been completed and you run the automate script it will not
re-run the simulation.  To force a recomputation, remove the output directory
for that simulation, for example::

    $ rm -rf outputs/elliptical_drop/std_sph
    $ ./automate.py -f EllipticalDrop

Adding more compute nodes
-------------------------

The automation framework in PySPH supports distributing the simulations to
other computers. One first needs to have password-less ssh access to these
computers. This is typically done by first creating an SSH key pair on the
local machine and copying that over to the remote machine. This is usually
done like so::

    $ ssh-keygen
    $ ssh-copy-id user@remote_machine

See the article `password less logins
<http://askubuntu.com/questions/46930/how-can-i-set-up-password-less-ssh-login>`_
for more details.

To setup a machine as a remote compute node, one must have a c++ compiler and
a Python installation on the machine. This should include the Python
development files (``python-dev`` or ``python-devel``). Given this, it is
fairly straightforward to setup a new machine as follows::

    $ python automate.py -a remote_host

This will copy the necessary files from the local computer to the remote
computer and setup everything on the remote machine. The configuration is
saved to the ``config.json`` file. The scripts for setting up and updating the
remote host will be placed in ``.automan`` and can be edited if your remote
machines require a different setup.

Once a new computer is added, when one runs the ``automate.py`` script it will
automatically distribute the computations across all the workers and the local
machine. If for some reason one does not wish to use a particular worker, one
can remove the entry from the ``config.json`` file.

If the sources are edited, one can update them on all the nodes by running::

   $ ./automate.py -u

Which will update the sources on all workers and rebuild PySPH as well as run
any problems specified.


Building the paper
-------------------

To build the paper::

   $ cd manuscript
   $ pdflatex edac_sph.tex
   $ bibtex edac_sph
   $ pdflatex edac_sph.tex
   $ pdflatex edac_sph.tex

Thats about it.

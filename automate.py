#!/usr/bin/env python
"""Script to automate the results using automan and pysph.

To learn more see the README.rst
"""

import os
import numpy as np
import matplotlib
matplotlib.use('pdf')

from automan.api import PySPHProblem as Problem   # noqa: 402
from automan.api import (Automator, Simulation, compare_runs,
                         filter_by_name, filter_cases) # noqa: 402
from pysph.solver.utils import load # noqa: 402


def get_files_at_given_times(files, times):
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if abs(t - times[count]) < t*1e-14:
            result.append(f)
            count += 1
        elif t > times[count]:
            count += 1
    return result


class TGV(Simulation):
    def get_command_line_args(self):
        extra = []
        map = {
            "wcsph": ["--scheme", "wcsph"],
            "wcsph_bql": ["--scheme", "wcsph", "--bql"],
            "wcsph_tensile_correction": [
                "--scheme", "wcsph", "--tensile-correction"
            ],
            "tvf_bql": ["--scheme", "tvf", "--bql"],
            "tvf": None,
            "tvf_zero_pb": None,
            "edac": None,
            "edac_ext": None,
            "edac_no_bql": ["--no-bql-edac"],
        }
        params = dict(self.params)
        params.pop('extra_label', None)
        scheme = params.pop('scheme', None)
        if scheme is not None:
            arg = map[scheme]
            if arg is not None:
                extra.extend(arg)

        return ' '.join(self.kwargs_to_command_line(params) + extra)

    def render_parameter(self, param):
        map = {
            "wcsph": "Standard SPH",
            "wcsph_bql": "SPH + BQL",
            "wcsph_tensile_correction": "SPH + tensile ",
            "tvf_bql": "TVF + BQL",
            "tvf": "TVF",
            "tvf_zero_pb": "TVF (pb=0)",
            "edac": "EDAC",
            "edac_ext": "EDAC ext",
            "edac_no_bql": "EDAC no-BQL"
        }
        if param not in self.params:
            return ''
        if param == 'alpha':
            return r'$\alpha=%s$' % self.params[param]
        if param == 'scheme':
            return map[self.params[param]]
        if param == 'extra_label':
            return self.params.get(param, '')
        else:
            return super(TGV, self).render_parameter(param)

    def decay_exact(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        label = kw.pop('label', 'Exact')
        plt.semilogy(data['t'], data['decay_ex'], label=label, **kw)

    def decay(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.semilogy(data['t'], data['decay'], **kw)
        plt.xlabel('t')
        plt.ylabel('max velocity')

    def l1(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['l1'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error')

    def linf(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['linf'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_\infty$ error')

    def p_l1(self, **kw):
        import matplotlib.pyplot as plt
        data = self.data
        plt.plot(data['t'], data['p_l1'], **kw)
        plt.xlabel('t')
        plt.ylabel(r'$L_1$ error for $p$')


class TaylorGreen(Problem):

    def get_name(self):
        return 'taylor_green'

    def setup(self):
        get_path = self.input_path
        pysph_cmd = 'pysph run taylor_green --openmp'
        edac_cmd = 'python code/taylor_green.py --openmp'
        self.cases = [
            TGV(
                get_path('std_sph'), pysph_cmd,
                job_info=dict(n_core=1, n_thread=2),
                scheme="wcsph", perturb=0.2, nx=50
            ),
            TGV(
                get_path('std_sph_bql'), edac_cmd,
                job_info=dict(n_core=1, n_thread=2),
                scheme="wcsph_bql", perturb=0.2, nx=50
            ),
            TGV(
                get_path('std_sph_tensile_correction'), pysph_cmd,
                job_info=dict(n_core=1, n_thread=2),
                scheme="wcsph_tensile_correction",
                perturb=0.2, nx=50
            ),

            TGV(
                get_path('tvf'), pysph_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.2, nx=50, scheme="tvf"
            ),
            TGV(
                get_path('tvf_zero_pb'), pysph_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.2, pb_factor=0.0, nx=50, scheme="tvf_zero_pb"
            ),
            TGV(
                get_path('tvf_bql'), edac_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.2, nx=50, scheme="tvf_bql"
            ),
            TGV(
                get_path('edac_ext'), edac_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.2, pb_factor=0.0,
                nx=50, scheme="edac_ext", alpha=0.5
            ),
            TGV(
                get_path('edac_int'), edac_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.2, pb_factor=1.0,
                nx=50, scheme="edac", alpha=0.5
            ),
            TGV(
                get_path('edac_nobql'), edac_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.2,
                pb_factor=1.0, nx=50, scheme="edac_no_bql", alpha=0.5
            ),
            # Convergence
            TGV(
                get_path('tvf_nx25'), pysph_cmd,
                job_info=dict(n_core=1, n_thread=1),
                perturb=0.1, nx=25,
                scheme='tvf'
            ),
            TGV(
                get_path('edac_nx25'), edac_cmd,
                job_info=dict(n_core=1, n_thread=1),
                perturb=0.1, nx=25,
                scheme='edac', alpha=0.5
            ),
            TGV(
                get_path('edac_nx25_plain'), edac_cmd,
                job_info=dict(n_core=1, n_thread=1),
                perturb=0.0, nx=25,
                scheme="edac", extra_label='No perturb', alpha=0.5
            ),
            TGV(
                get_path('edac_nx51_plain'), edac_cmd,
                job_info=dict(n_core=1, n_thread=2),
                perturb=0.0, nx=51,
                scheme="edac", alpha=0.5
            ),
            TGV(
                get_path('edac_nx101_plain'), edac_cmd,
                job_info=dict(n_core=2, n_thread=4),
                perturb=0.0, nx=101,
                scheme="edac", alpha=0.5
            ),
            TGV(
                get_path('tvf_nx100'), pysph_cmd,
                job_info=dict(n_core=2, n_thread=4),
                perturb=0.2, nx=100, scheme="tvf"
            ),
            TGV(
                get_path('edac_nx100'), edac_cmd,
                job_info=dict(n_core=2, n_thread=4),
                perturb=0.2, nx=100,
                scheme="edac", alpha=0.5
            ),
        ]

    def run(self):
        self.make_output_dir()
        self._plot_decay_error_all()
        self._plot_decay_error()
        self._plot_convergence()
        self._plot_particles()
        self._plot_density_variation()

    def _plot_decay_error_all(self):
        import matplotlib.pyplot as plt

        cases = filter_cases(self.cases, nx=50, perturb=0.2)
        compare_runs(cases, 'decay', labels=['scheme'], exact='decay_exact')
        plt.legend()
        plt.savefig(self.output_path('decay_all.pdf'))
        plt.close()

        plt.figure()
        compare_runs(cases, 'l1', labels=['scheme'])
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('l1_error_all.pdf'))
        plt.close()

        compare_runs(cases, 'linf', labels=['scheme'])
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('linf_error_all.pdf'))
        plt.close()

    def _plot_decay_error(self):
        import matplotlib.pyplot as plt

        cases = filter_by_name(
            self.cases, ['std_sph', 'tvf', 'edac_ext', 'edac_int']
        )
        compare_runs(cases, 'decay', labels=['scheme'], exact='decay_exact')
        plt.legend()
        plt.axis('tight')
        plt.savefig(self.output_path('decay.pdf'))
        plt.close()

        plt.figure()
        compare_runs(cases, 'l1', labels=['scheme'])
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('l1_error.pdf'))
        plt.close()

        plt.figure()
        compare_runs(cases, 'p_l1', labels=['scheme'])
        plt.legend(loc='upper left')
        plt.ylim(0, 10)
        plt.savefig(self.output_path('p_l1_error.pdf'))
        plt.close()

        cases = filter_by_name(
            self.cases,
            ['tvf', 'tvf_zero_pb', 'tvf_bql', 'edac_int', 'edac_nobql']
        )
        compare_runs(cases, 'l1', labels=['scheme'])
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('l1_error_other.pdf'))
        plt.close()

    def _plot_convergence(self):
        import matplotlib.pyplot as plt
        cases = filter_by_name(
            self.cases, ['edac_nx25', 'edac_int', 'edac_nx100', 'tvf_nx25',
                         'tvf', 'tvf_nx100']
        )
        compare_runs(cases, 'l1', labels=['scheme', 'nx', 'extra_label'])
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('l1_error_conv.pdf'))
        plt.close()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files, load
        for name in ('edac_nx100', 'tvf_nx100'):
            case = filter_by_name(self.cases, [name])[0]
            files = get_files(case.input_path(), 'taylor_green')
            # At 11000, the time value is 2.5.
            fname = [f for f in files if '11000' in f]
            data = load(fname[0])
            sd = data['solver_data']
            assert abs(sd['t'] - 2.5) < 1e-8, 'file: %s, %s' % (fname, sd)
            f = data['arrays']['fluid']
            for scalar in ('p', 'vmag'):
                if scalar == 'vmag':
                    c = np.sqrt(f.u*f.u + f.v*f.v)
                else:
                    c = f.p - np.average(f.p)
                plt.scatter(
                    f.x, f.y, c=c, marker='.', s=5,
                    cmap='jet', edgecolors='none'
                )
                plt.colorbar(label=scalar)
                plt.axis('equal')
                plt.xlabel('x')
                plt.ylabel('y')
                plt.tight_layout()
                name_base = '%s_%s' % (name, scalar)
                plt.savefig(self.output_path(name_base + '.pdf'))
                plt.savefig(self.output_path(name_base + '.png'), dpi=150)
                plt.close()

    def _plot_density_variation(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files, load
        cases = {
            'std_sph': ('WCSPH', 'k--'),
            'edac_int': ('EDAC', 'k-'),
            'tvf': ('TVF', 'k:')
        }
        self._eval = None
        for name, (label, linestyle) in cases.items():
            case = filter_by_name(self.cases, name)[0]
            files = get_files(case.input_path(), 'taylor_green')
            rho_diff = []
            time = []
            for fname in files:
                data = load(fname)
                pa = data['arrays']['fluid']
                time.append(data['solver_data']['t'])
                if name == 'std_sph':
                    self._compute_rho_sd(pa)
                delta = pa.rho.max() - pa.rho.min()
                rho_diff.append(delta)
            plt.plot(time, rho_diff, linestyle, label=label)

        plt.xlabel('t')
        plt.ylabel(r'$\rho_{max} - \rho_{min}$')
        plt.legend(loc='center right')
        plt.savefig(self.output_path('density_variation.pdf'))
        plt.close()

    def _get_evaluator(self, pa):
        if self._eval is not None:
            return self._eval
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.base.kernels import QuinticSpline
        from pysph.base.nnps import DomainManager
        from pysph.sph.basic_equations import SummationDensity
        equations = [
            SummationDensity(dest='fluid', sources=['fluid'])
        ]
        dm = DomainManager(
            xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0, periodic_in_x=True,
            periodic_in_y=True
        )
        self._eval = SPHEvaluator(
            arrays=[pa], equations=equations, dim=2,
            kernel=QuinticSpline(dim=2), domain_manager=dm
        )
        return self._eval

    def _compute_rho_sd(self, pa):
        """Compute rho using summation density."""
        sph_eval = self._get_evaluator(pa)
        sph_eval.update_particle_arrays([pa])
        sph_eval.evaluate()


class TaylorGreenRe(Problem):

    def setup(self):
        cmd = 'python code/taylor_green.py --openmp'
        pysph_cmd = 'pysph run taylor_green --openmp'
        get_path = self.input_path
        self.cases = [
            # TGV(get_path('edac_re100'),  cmd,
            #    job_info=dict(n_core=1, n_thread=1),
            #    re=100, nx=25, alpha=0),
            TGV(
                get_path('edac_re100_alpha_0.01'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=0.01
            ),
            TGV(
                get_path('edac_re100_alpha_0.05'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=0.05
            ),
            TGV(
                get_path('edac_re100_alpha_0.1'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=0.1
            ),
            TGV(
                get_path('edac_re100_alpha_0.2'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=0.2
            ),
            TGV(
                get_path('edac_re100_alpha_0.5'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=0.5
            ),
            TGV(
                get_path('edac_re100_alpha_1'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=1.0
            ),
            TGV(
                get_path('edac_re100_alpha_2'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                re=100, nx=25, alpha=2.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1'),  cmd,
                job_info=dict(n_core=1, n_thread=1),
                nx=25, re=1000, alpha=1.0, tf=5.0, hdx=1.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1_nx_51'),  cmd,
                job_info=dict(n_core=1, n_thread=2),
                nx=51, re=1000, alpha=1.0, tf=5.0, hdx=1.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1_nx_101'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=101, re=1000, alpha=1.0, tf=5.0, hdx=1.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1_nx_151'),  cmd,
                job_info=dict(n_core=2, n_thread=8),
                nx=151, re=1000, alpha=1.0, tf=5.0, pfreq=500, hdx=1.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1_nx_201'),  cmd,
                job_info=dict(n_core=4, n_thread=8),
                nx=201, re=1000, alpha=1.0, tf=5.0, pfreq=500,
                hdx=1.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1_nx_251'),  cmd,
                job_info=dict(n_core=4, n_thread=8),
                nx=251, re=1000, alpha=1.0, tf=5.0, pfreq=1000,
                hdx=1.0
            ),
            TGV(
                get_path('edac_re1000_alpha_1_nx_301'),  cmd,
                job_info=dict(n_core=4, n_thread=8),
                nx=301, re=1000, alpha=1.0, tf=5.0, pfreq=1000, hdx=1.0
            ),
            TGV(
                get_path('edac_re10000_alpha_0'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=101, re=10000, alpha=0.0, tf=10.0
            ),
            TGV(
                get_path('edac_re10000_alpha_0.5'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=101, re=10000, alpha=0.5, tf=10.0
            ),
            TGV(
                get_path('edac_re10000_alpha_1'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=101, re=10000, alpha=1.0, tf=10.0
            ),
            TGV(
                get_path('edac_re10000_alpha_2'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=101, re=10000, alpha=2.0, tf=10.0
            ),
            # Re 10k comparison
            TGV(
                get_path('edac_re10k'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=100, pb_factor=1.0,
                re=10000, alpha=0.5, tf=10.0, perturb=0.2, scheme='edac'
            ),
            TGV(
                get_path('edac_ext_re10k'),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=100, pb_factor=0.0,
                re=10000, alpha=0.5, tf=10.0, perturb=0.2, scheme='edac_ext'
            ),
            TGV(
                get_path('tvf_re10k'),  pysph_cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=100, re=10000, tf=10.0, perturb=0.2, scheme='tvf'
            ),
        ]

    def get_name(self):
        return 'taylor_green_re'

    def run(self):
        self.make_output_dir()
        self._plot_decay_error()
        self._plot_convergence_rate()
        self._plot_particles()

    def _plot_decay_error(self):
        import matplotlib.pyplot as plt
        comparisons = {
            're100': dict(params=dict(re=100, nx=25), labels=['alpha']),
            're1000_conv': dict(params=dict(re=1000, alpha=1.0),
                                labels=['nx']),
            're10k': dict(params=dict(re=10000, nx=101), labels=['alpha']),
            're10k_comp': dict(params=dict(re=10000, nx=100, perturb=0.2),
                               labels=['scheme']
                               )
        }

        for key, info in comparisons.items():
            labels = info['labels']
            params = info['params']
            cases = filter_cases(self.cases, **params)
            compare_runs(cases, 'decay', labels=labels, exact='decay_exact')
            plt.legend()
            plt.savefig(self.output_path('decay_%s.pdf' % key))
            plt.close()

            plt.figure()
            compare_runs(cases, 'l1', labels=labels)
            plt.legend(loc='upper left')
            plt.savefig(self.output_path('l1_error_%s.pdf' % key))
            plt.close()

    def _plot_convergence_rate(self):
        import matplotlib.pyplot as plt
        cases = filter_cases(self.cases, re=1000, alpha=1.0)
        data = {}
        for case in cases:
            l1 = case.data['l1'][-1]
            data[case.params['nx']] = l1
        nx = np.asarray(sorted(data.keys()), dtype=float)
        l1 = np.asarray([data[x] for x in nx])
        plt.figure()
        plt.loglog(1.0/nx, l1, 'k-o', label='Computed EDAC')
        plt.loglog(1.0/nx, (30./nx), 'k--', linewidth=2,
                   label=r'Expected $O(h)$')
        plt.legend(loc='upper left')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        plt.xlim(0.0025, 0.04)
        plt.savefig(self.output_path('l1_re1000_loglog_conv.pdf'))
        plt.close()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files, load
        cases = filter_by_name(self.cases, ['edac_re1000_alpha_1_nx_201'])
        case = cases[0]
        files = get_files(case.input_path(), 'taylor_green')
        data = load(files[-1])
        f = data['arrays']['fluid']
        c = np.sqrt(f.u*f.u + f.v*f.v)
        plt.scatter(
            f.x, f.y, c=c, marker='.', s=5,
            cmap='jet', edgecolors='none'
        )
        plt.colorbar(label='vmag')
        plt.axis('equal')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.tight_layout()
        name_base = 're1000_nx201_vmag'
        plt.savefig(self.output_path(name_base + '.pdf'))
        plt.savefig(self.output_path(name_base + '.png'), dpi=150)
        plt.close()


class TaylorGreenConv(Problem):

    def setup(self):
        cmd = (
            'python code/taylor_green.py --openmp --tf=2.5 --perturb=0.2 '
            '--re=1000 --kernel WendlandQuintic'
        )
        get_path = self.input_path
        self.hdx = (1.5, 1.75, 2.0, 2.25, 2.5)
        self.nx = (25, 50, 100, 150, 200)
        self.cases = [
            TGV(get_path('nx_%d_hdx_%.2f' % (nx, hdx)),  cmd,
                job_info=dict(n_core=2, n_thread=4),
                nx=nx, hdx=hdx, alpha=0.5)
            for nx in self.nx
            for hdx in self.hdx
        ]

    def get_name(self):
        return 'taylor_green_conv'

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate()

    def _plot_convergence_rate(self):
        import matplotlib.pyplot as plt
        plt.figure()
        linestyles = ['k-o', 'k--o', 'k-.o', 'k:o', 'b-o']
        for i, hdx in enumerate(self.hdx):
            cases = filter_cases(self.cases, hdx=hdx)
            data = {}
            for case in cases:
                l1 = case.data['l1'][-1]
                data[case.params['nx']] = l1
            nx = np.asarray(sorted(data.keys()), dtype=float)
            l1 = np.asarray([data[x] for x in nx])
            plt.loglog(1.0/nx, l1, linestyles[i],
                       label=r'$h/\Delta x$=%.2f' % hdx)
        plt.loglog(1.0/nx, (30./nx)**2, 'r-', linewidth=2,
                   label=r'Expected $O(h)$')
        plt.legend(loc='upper left')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        # plt.xlim(0.0025, 0.04)
        plt.savefig(self.output_path('l1_loglog_conv.pdf'))
        plt.close()


class Couette(Problem):
    def get_name(self):
        return 'couette'

    def get_commands(self):
        return [
            ('tvf', 'pysph run couette --openmp', None,),
            ('edac', 'python code/couette.py --openmp', None),
        ]

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()

    def _plot_u_profile(self):
        import numpy as np
        import matplotlib.pyplot as plt
        tvf = np.load(self.input_path('tvf', 'results.npz'))
        edac = np.load(self.input_path('edac', 'results.npz'))

        plt.plot(tvf['y_ex'][::2], tvf['u_ex'][::2], 'k-',
                 label='Exact')
        plt.plot(tvf['y'], tvf['u'], 'ko', fillstyle='none',
                 label='TVF')
        plt.plot(edac['y'], edac['u'], 'ks', fillstyle='none',
                 label='EDAC')
        plt.legend(loc='upper left')
        plt.xlim(0.0, 1.0)
        plt.ylim(0.0, None)
        plt.xlabel(r'$y$')
        plt.ylabel(r'$u$')
        plt.savefig(self.output_path('u_vs_y.pdf'))
        plt.close()


class Poiseuille(Couette):
    def get_name(self):
        return 'poiseuille'

    def get_commands(self):
        return [
            ('tvf', 'pysph run poiseuille --openmp',
             dict(n_core=1, n_thread=2)),
            ('edac', 'python code/poiseuille.py --openmp',
             dict(n_core=1, n_thread=2)),
            ('edac_int', 'python code/poiseuille.py --openmp --use-pb',
             dict(n_core=1, n_thread=2)),
        ]


class Cavity(Problem):
    def get_name(self):
        return 'cavity'

    def get_commands(self):
        cmd = 'python code/ldc.py --openmp '
        return [
            ('edac_re100_nx25',
             cmd + '--nx 25 --re 100 --tf 25 --n-vel-avg=10',
             dict(n_core=2, n_thread=4)),
            ('edac_re100_nx50',
             cmd + '--nx 50 --re 100 --tf 25 --n-vel-avg=10',
             dict(n_core=2, n_thread=4)),
            ('edac_re1k_nx50',
             cmd + '--nx 50 --re 1000 --tf 60 --n-vel-avg=20',
             dict(n_core=4, n_thread=8)),
            ('edac_re1k_nx100',
             cmd + '--nx 100 --re 1000 --tf 60 --n-vel-avg=20',
             dict(n_core=4, n_thread=8)),
        ]

    def run(self):
        self.make_output_dir()
        self._plot_u_profile()

    def _plot_u_profile(self):
        import numpy as np
        import matplotlib.pyplot as plt
        re100 = np.load(self.input_path('edac_re100_nx25', 'results.npz'))
        re100_2 = np.load(self.input_path('edac_re100_nx50', 'results.npz'))
        re1k = np.load(self.input_path('edac_re1k_nx50', 'results.npz'))
        re1k_2 = np.load(self.input_path('edac_re1k_nx100', 'results.npz'))
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        # Re 100
        # u vs y
        y, exp = get_u_vs_y()
        s1 = plt.subplot(211)
        s1.plot(re100_2['u_c'], re100_2['x'], 'k-',
                label='Computed (Re=100, nx=50)')
        s1.plot(re100['u_c'], re100['x'], 'k--',
                label='Computed (Re=100, nx=25)')
        s1.plot(exp[100], y, 'ko', fillstyle='none',
                label='Ghia et al. (Re=100)')
        s1.set_xlabel('$u$')
        s1.set_ylabel('$y$')
        s1.legend()  # loc='lower right')

        x, exp = get_v_vs_x()
        s2 = plt.subplot(212)
        s2.plot(re100_2['x'], re100_2['v_c'], 'k-',
                label='Computed (Re=100, nx=50)')
        s2.plot(re100['x'], re100['v_c'], 'k--',
                label='Computed (Re=100, nx=25)')
        s2.plot(x, exp[100], 'ko', fillstyle='none',
                label='Ghia et al. (Re=100)')
        s2.set_xlabel('$x$')
        s2.set_ylabel('$v$')
        s2.legend()
        plt.subplots_adjust(hspace=0.3)
        plt.savefig(self.output_path('uv_re100.pdf'))
        plt.close()

        # Re 1000
        # u vs y
        y, exp = get_u_vs_y()
        s1 = plt.subplot(211)
        s1.plot(re1k_2['u_c'], re1k_2['x'], 'k-',
                label='Computed (Re=1000, nx=100)')
        s1.plot(re1k['u_c'], re1k['x'], 'k--',
                label='Computed (Re=1000, nx=50)')
        s1.plot(exp[1000], y, 'ko', fillstyle='none',
                label='Ghia et al. (Re=1000)')
        s1.set_xlabel('$u$')
        s1.set_ylabel('$y$')
        s1.legend()  # loc='lower right')

        x, exp = get_v_vs_x()
        s2 = plt.subplot(212)
        s2.plot(re1k_2['x'], re1k_2['v_c'], 'k-',
                label='Computed (Re=1000, nx=100)')
        s2.plot(re1k['x'], re1k['v_c'], 'k--',
                label='Computed (Re=1000, nx=50)')
        s2.plot(x, exp[1000], 'ko', fillstyle='none',
                label='Ghia et al. (Re=1000)')
        s2.set_xlabel('$x$')
        s2.set_ylabel('$v$')
        s2.legend()
        plt.subplots_adjust(hspace=0.3)
        plt.savefig(self.output_path('uv_re1000.pdf'))
        plt.close()


class LatticeCylinders(Problem):
    def get_name(self):
        return 'lattice_cylinders'

    def setup(self):
        get_path = self.input_path
        self.cases = [
            Simulation(
                get_path('edac'), 'python code/lattice_cylinders.py --openmp',
                job_info=dict(n_core=2, n_thread=4)
            ),
            Simulation(
                get_path('tvf'), 'pysph run lattice_cylinders --openmp',
                job_info=dict(n_core=2, n_thread=4)
            ),
        ]

    def run(self):
        import matplotlib.pyplot as plt
        self.make_output_dir()
        tvf = self.cases[1]
        edac = self.cases[0]
        data = tvf.data
        plt.plot(data['y'], data['ui_lby2'], 'k-', label='x=L/2 (TVF)')
        plt.plot(data['y'], data['ui_l'], 'k--', label='x=L (TVF)')
        data = edac.data
        plt.plot(data['y'], data['ui_lby2'], 'r-', label='x=L/2 (EDAC)')
        plt.plot(data['y'], data['ui_l'], 'r--', label='x=L (EDAC)')
        plt.ylim(-5e-6, 1.2e-4)

        plt.xlabel('y/H')
        plt.ylabel('u')
        plt.legend()
        plt.savefig(self.output_path('u_profile.pdf'))
        plt.close()


class PeriodicCylinders(Problem):
    def get_name(self):
        return 'periodic_cylinders'

    def setup(self):
        get_path = self.input_path
        self.cases = [
            Simulation(
                get_path('edac'), 'python code/periodic_cylinders.py --openmp',
                job_info=dict(n_core=2, n_thread=4)
            ),
            Simulation(
                get_path('tvf'), 'pysph run periodic_cylinders --openmp',
                job_info=dict(n_core=2, n_thread=4)
            ),
        ]

    def run(self):
        import matplotlib.pyplot as plt
        self.make_output_dir()
        tvf = self.cases[1]
        edac = self.cases[0]
        data = tvf.data
        plt.plot(data['t'], data['cd'], 'k--', label='TVF')
        data = edac.data
        plt.plot(data['t'], data['cd'], 'k-', label='EDAC')
        plt.xlabel(r'$t$')
        plt.ylabel(r'$C_D$')
        plt.ylim(100, 130)
        plt.xlim(0, 400)
        plt.legend()
        plt.savefig(self.output_path('cd_vs_t.pdf'))
        plt.close()
        self._plot_particles()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files, load
        edac = self.cases[0]
        files = get_files(edac.input_path(), 'periodic_cylinders')
        data = load(files[-1])
        f = data['arrays']['fluid']
        plt.scatter(
            f.x, f.y, c=np.sqrt(f.u*f.u + f.v*f.v), marker='.', s=5,
            cmap='jet', edgecolors='none'
        )
        plt.colorbar()
        plt.axis([0.0, 0.12, 0.0, 0.08])
        plt.xlabel('x')
        plt.ylabel('y')
        plt.savefig(self.output_path('edac_particles.pdf'))
        plt.savefig(self.output_path('edac_particles.png'), dpi=150)
        plt.close()


class EllipticalDrop(Problem):

    def get_name(self):
        return 'elliptical_drop'

    def get_commands(self):
        return [
            ("std_sph",
             "pysph run elliptical_drop --openmp --no-adaptive-timestep",
             dict(n_core=2, n_thread=4)),
            ("edac",
             "python code/elliptical_drop.py --openmp --alpha=0.5",
             dict(n_core=2, n_thread=4)),
            ("edac_xsph",
             "python code/elliptical_drop.py --openmp --xsph-eps=0.5",
             dict(n_core=2, n_thread=4)),
            ("edac_xsph_x2",
             "python code/elliptical_drop.py --openmp --xsph-eps=0.5 --nx 81",
             dict(n_core=2, n_thread=4)),
            ("edac_xsph_x3",
             "python code/elliptical_drop.py --openmp --xsph-eps=0.5 --nx 120",
             dict(n_core=2, n_thread=4)),
        ]

    def run(self):
        self.make_output_dir()
        self._plot_axis_and_ke()
        self._plot_particles()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import load, get_files
        from pysph.examples.elliptical_drop import exact_solution
        for name in ('std_sph', 'edac', 'edac_xsph'):
            files = get_files(self.input_path(name), 'elliptical_drop')
            data = load(files[-1])
            pa = data['arrays']['fluid']
            a, A, po, xe, ye = exact_solution(data['solver_data']['t'])
            plt.figure(figsize=(5, 10))
            plt.axis('equal')
            plt.plot(xe, ye, 'k-')
            plt.scatter(
                pa.x, pa.y, c=pa.p, marker='.', s=10,
                edgecolors='none', linewidth=0, cmap='jet'
            )
            plt.colorbar()
            plt.ylim(-2, 2)
            plt.xlabel('x')
            plt.ylabel('y')
            plt.savefig(self.output_path(name + '.png'), dpi=150)
            plt.savefig(self.output_path(name + '.pdf'))
            plt.close()

    def _plot_axis_and_ke(self):
        import numpy as np
        import matplotlib.pyplot as plt
        sph = np.load(self.input_path('std_sph', 'results.npz'))
        edac = np.load(self.input_path('edac', 'results.npz'))
        edac_xsph = np.load(self.input_path('edac_xsph', 'results.npz'))
        edac_xsph2 = np.load(self.input_path('edac_xsph_x2', 'results.npz'))
        edac_xsph3 = np.load(self.input_path('edac_xsph_x3', 'results.npz'))

        plt.plot(edac['t'], np.abs(edac['ymax'] - edac['major']),
                 'k:', label='EDAC')
        plt.plot(sph['t'], np.abs(sph['ymax'] - sph['major']),
                 'k-.', label='SPH')
        plt.plot(edac_xsph['t'],
                 np.abs(edac_xsph['ymax'] - edac_xsph['major']),
                 'k-', label='EDAC (XSPH)')
        plt.plot(edac_xsph2['t'],
                 np.abs(edac_xsph2['ymax'] - edac_xsph2['major']),
                 'b-', label=r'EDAC (XSPH) $\Delta x/2$')
        plt.plot(edac_xsph3['t'],
                 np.abs(edac_xsph3['ymax'] - edac_xsph3['major']),
                 'g-', label=r'EDAC (XSPH) $\Delta x/3$')
        plt.xlabel(r't')
        plt.ylabel('Error in major-axis')
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('major_axis.pdf'))
        plt.close()

        plt.plot(edac['t'], edac['ke'], 'k:', label='EDAC')
        plt.plot(sph['t'], sph['ke'], 'k-.', label='SPH')
        plt.plot(edac_xsph['t'], edac_xsph['ke'], 'k-', label='EDAC (XSPH)')
        plt.plot(edac_xsph2['t'], edac_xsph2['ke'], 'b-',
                 label=r'EDAC (XSPH) $\Delta x/2$')
        plt.plot(edac_xsph3['t'], edac_xsph3['ke'], 'g-',
                 label=r'EDAC (XSPH) $\Delta x/3$')
        plt.xlabel(r't')
        plt.ylabel('Kinetic Energy')
        plt.legend(loc='lower left')
        plt.savefig(self.output_path('ke.pdf'))
        plt.close()


class DamBreak2D(Problem):

    def get_name(self):
        return 'dam_break2d'

    def get_commands(self):
        return [
            ("std_sph",
             "pysph run dam_break_2d --openmp --tf=1.0 --h-factor=2.5",
             dict(n_core=4, n_thread=8)),
            ("edac_r1", "python code/dam_break.py --openmp --tf=1.0",
             dict(n_core=2, n_thread=4)),
            ("edac_r2",
             "python code/dam_break.py --openmp --h-factor=1.5 --tf=1.0",
             dict(n_core=2, n_thread=4)),
            ("edac_r3",
             "python code/dam_break.py --openmp --h-factor=2.5 --tf=1.0",
             dict(n_core=4, n_thread=8)),
        ]

    def run(self):
        self.make_output_dir()
        self._plot_toe()
        self._plot_particles()

    def _plot_toe(self):
        import matplotlib.pyplot as plt
        sph = np.load(self.input_path('std_sph', 'results.npz'))
        edac1 = np.load(self.input_path('edac_r1', 'results.npz'))
        edac2 = np.load(self.input_path('edac_r2', 'results.npz'))
        edac3 = np.load(self.input_path('edac_r3', 'results.npz'))
        from pysph.examples import db_exp_data as dbd
        plt.plot(edac1['t'], edac1['x_max'], 'k:', label='EDAC (h=0.039)')
        plt.plot(edac2['t'], edac2['x_max'], 'k--', label='EDAC (h=0.026)')
        plt.plot(edac3['t'], edac3['x_max'], 'k-', label='EDAC (h=0.0156)')
        plt.plot(sph['t'], sph['x_max'], 'k-.', label='SPH (h=0.0156)')
        tmps, xmps = dbd.get_koshizuka_oka_mps_data()
        plt.plot(tmps, xmps, '^', label='MPS Koshizuka & Oka (1996)')
        factor = np.sqrt(2.0*9.81/1.0)
        plt.xlim(0, 0.7*factor)
        plt.ylim(0.5, 4.5)
        plt.xlabel(r'$T$')
        plt.ylabel(r'$Z/L$')
        plt.legend(loc='upper left')
        plt.savefig(self.output_path('toe_vs_t.pdf'))
        plt.close()

    def _plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import load, get_files
        for name in ('std_sph', 'edac_r3'):
            fname = 'dam_break' if name.startswith('edac') else 'dam_break_2d'
            files = get_files(self.input_path(name), fname)
            times = [0.4, 0.6, 0.8, 1.0]
            to_plot = get_files_at_given_times(files, times)
            for i, f in enumerate(to_plot):
                data = load(f)
                pa = data['arrays']['fluid']
                for scalar in ('p', 'vmag'):
                    plt.axis('equal')
                    if scalar == 'p':
                        c = pa.p
                    else:
                        c = np.sqrt(pa.u*pa.u + pa.v*pa.v)
                    plt.scatter(
                        pa.x, pa.y, c=c, marker='.', s=5,
                        edgecolors='none', cmap='jet', linewidth=0
                    )
                    plt.colorbar(label=scalar)
                    pa1 = data['arrays']['boundary']
                    plt.scatter(
                        pa1.x, pa1.y, c=pa1.m, marker='.', s=5,
                        edgecolors='none', linewidth=0
                    )
                    plt.annotate(
                        't = %.1f' % times[i], xy=(0.5, 3.5), fontsize=18
                    )
                    plt.xlim(-0.1, 4.1)
                    plt.ylim(-0.1, 4.1)
                    plt.xlabel('x')
                    plt.ylabel('y')
                    plt.tight_layout()
                    fbase = name + '_%s_%d' % (scalar, i)
                    plt.savefig(self.output_path(fbase + '.pdf'))
                    plt.savefig(self.output_path(fbase + '.png'), dpi=150)
                    plt.close()


class HydrostaticTank(Problem):
    def get_name(self):
        return 'h_tank'

    def get_commands(self):
        return [
            ('tvf', 'pysph run hydrostatic_tank --openmp',
             dict(n_core=2, n_thread=4)),
            ('edac', 'python code/hydrostatic_tank.py --openmp',
             dict(n_core=2, n_thread=4)),
        ]

    def run(self):
        self.make_output_dir()
        self._make_plots()

    def _make_plots(self):
        import numpy as np
        import matplotlib.pyplot as plt
        tvf = np.load(self.input_path('tvf', 'results.npz'))
        edac = np.load(self.input_path('edac', 'results.npz'))

        plt.plot(tvf['t'], tvf['p'][:, 0], 'k--o',
                 fillstyle='none', label='TVF')
        plt.plot(edac['t'], edac['p'][:, 0], 'k-^',
                 fillstyle='none', label='EDAC')
        plt.legend(loc='upper left')
        plt.xlabel(r'$t$')
        plt.ylabel(r'$p$')
        plt.savefig(self.output_path('p_bottom.pdf'))
        plt.close()

        output_at = [0.5, 2.0]
        pmax = 0.9*1000.*1.0
        t = tvf['t']
        count = 0
        for i in range(len(t)):
            if abs(t[i] - output_at[count]) < 1e-8:
                y = tvf['y']
                plt.plot(
                    y, tvf['p'][i]/pmax, 'ko', fillstyle='none',
                )
                plt.plot(
                    y, edac['p'][i]/pmax, 'k^', fillstyle='none',
                )
                plt.plot(
                    y, edac['p_ex'][i]*(0.9 - y)/(0.9*pmax), 'k-',
                )
                count += 1
        plt.xlabel('$y$')
        plt.ylabel('$p$')
        plt.annotate('$t=0.5$', xy=(0.2, 0.45))
        plt.annotate('$t=2.0$', xy=(0.41, 0.6))
        plt.legend(['TVF', 'EDAC', 'Exact'])
        plt.savefig(self.output_path('p_vs_y.pdf'))
        plt.close()


class WaveMaker(Problem):
    def get_name(self):
        return 'wave_maker'

    def get_commands(self):
        return [
            ('wcsph', 'python code/wave_maker.py --scheme=wcsph --openmp',
             dict(n_core=2, n_thread=4)),
            ('edac', 'python code/wave_maker.py --scheme=edac --openmp',
             dict(n_core=2, n_thread=4)),
            ('edac_no_avisc',
             'python code/wave_maker.py --openmp --scheme=edac --alpha=0.0',
             dict(n_core=2, n_thread=4)),
            ('aha', 'python code/wave_maker.py --scheme=aha --openmp',
             dict(n_core=2, n_thread=4)),
        ]

    def run(self):
        self.make_output_dir()

        import numpy as np
        import matplotlib.pyplot as plt
        wcsph = np.load(self.input_path('wcsph', 'results.npz'))
        edac = np.load(self.input_path('edac', 'results.npz'))
        aha = np.load(self.input_path('aha', 'results.npz'))
        edac_no_avisc = np.load(
            self.input_path('edac_no_avisc', 'results.npz')
        )

        plt.plot(edac['y'], edac['p'], 'k-', label='EDAC')
        plt.plot(wcsph['y'], wcsph['p'], 'k--', label='WCSPH')
        plt.xlabel('y')
        plt.ylabel('$p$')
        plt.legend()
        plt.savefig(self.output_path('p_vs_y.pdf'))
        plt.close()

        cases = {'EDAC': edac, 'WCSPH': wcsph,
                 'EDAC_no_avisc': edac_no_avisc, 'Adami Hu Adams': aha}

        with open(self.output_path('results.csv'), 'w') as f:
            f.write('name,pmin,pmax\n')
            for name, case in cases.items():
                f.write('%s,%f,%f\n' % (name, case['p_min'], case['p_max']))


class TwoBlocks(Problem):
    def get_name(self):
        return 'two_blocks'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/two_blocks.py --openmp'
        self.cases = [
            Simulation(
                get_path('wcsph'), cmd,
                job_info=dict(n_core=2, n_thread=4),
                scheme='wcsph', alpha=0.1, nx=100, mach_number=0.01
            ),
            Simulation(
                get_path('edac'), cmd,
                job_info=dict(n_core=2, n_thread=4),
                scheme='edac', alpha=0.1, nx=100, mach_number=0.01
            ),
            Simulation(
                get_path('edac_noavisc'), cmd,
                job_info=dict(n_core=2, n_thread=4),
                scheme='edac', alpha=0.0, nx=100, mach_number=0.01
            ),
            Simulation(
                get_path('wcsph_noavisc'), cmd,
                job_info=dict(n_core=2, n_thread=4),
                scheme='wcsph', alpha=0.0, nx=100, mach_number=0.01, tf=0.1
            ),
        ]

    def run(self):
        self.make_output_dir()
        import shutil
        for case in ('wcsph', 'edac', 'edac_noavisc', 'wcsph_noavisc'):
            shutil.copy(self.input_path(case, 'p_t007.pdf'),
                        self.output_path('%s_p_t007.pdf' % case))
            shutil.copy(self.input_path(case, 'p_t007.png'),
                        self.output_path('%s_p_t007.png' % case))
            shutil.copy(self.input_path(case, 'p_final.pdf'),
                        self.output_path('%s_p_final.pdf' % case))
            shutil.copy(self.input_path(case, 'p_final.png'),
                        self.output_path('%s_p_final.png' % case))


# Note that some of these problems are not used in the actual manuscript.
all_problems = [
    TaylorGreen, TaylorGreenRe, TaylorGreenConv, Couette, Poiseuille, Cavity,
    LatticeCylinders, PeriodicCylinders, EllipticalDrop, DamBreak2D,
    HydrostaticTank, WaveMaker, TwoBlocks
]


if __name__ == '__main__':
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=all_problems
    )
    automator.run()

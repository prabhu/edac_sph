from pysph.examples.elliptical_drop import EllipticalDrop
from edac import EDACScheme


class EDACEllipticalDrop(EllipticalDrop):
    def initialize(self):
        super(EDACEllipticalDrop, self).initialize()
        self.hdx = 1.2

    def add_user_options(self, group):
        super(EDACEllipticalDrop, self).add_user_options(group)
        group.add_argument(
            "--xsph-eps", action="store", type=float, dest="xsph_eps",
            default=0.0,
            help="eps factor to use for XSPH correction."
        )
        group.add_argument(
            "--alpha", action="store", type=float, dest='alpha',
            default=0.5,
            help="Viscosity factor to use (multiple of rho*h*c/8)."
        )

    def configure_scheme(self):
        self.scheme.configure(
            eps=self.options.xsph_eps, alpha=self.options.alpha,
            h=self.hdx*self.dx
        )
        dt = 0.25*self.hdx*self.dx/(141 + self.co)
        tf = 0.0076
        self.scheme.configure_solver(
            dt=dt, tf=tf, adaptive_timestep=False
        )

    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=[], dim=2, c0=self.co, nu=0.0,
            h=self.hdx*self.dx, rho0=self.ro, pb=0.0, eps=0.0, clamp_p=False
        )
        return scheme

    def create_particles(self):
        [pa] = super(EDACEllipticalDrop, self).create_particles()
        # One must set the pessure to zero, otherwise the particles will just
        # go apart due to the pressure.
        pa.p[:] = 0.0
        return [pa]


if __name__ == '__main__':
    app = EDACEllipticalDrop()
    app.run()
    app.post_process(app.info_filename)

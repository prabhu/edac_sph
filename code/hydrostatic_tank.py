from pysph.examples import hydrostatic_tank

from edac import EDACScheme


class EDACHT(hydrostatic_tank.HydrostaticTank):
    def add_user_options(self, group):
        # Don't add parent user options as we don't need them.
        pass

    def create_scheme(self):
        c0 = hydrostatic_tank.c0
        h0 = hydrostatic_tank.h0
        rho0 = hydrostatic_tank.rho0
        scheme = EDACScheme(
            fluids=['fluid'], solids=['solid'], dim=2, c0=c0, rho0=rho0,
            nu=0.0, pb=0.0, gy=-1.0, eps=0.0, h=h0, alpha=0.5, tdamp=1.0,
            artificial_alpha=0.24, clamp_p=False
        )
        tf = hydrostatic_tank.tf
        dt = hydrostatic_tank.dt
        print("dt = %g"%dt)
        scheme.configure_solver(
            dt=dt, tf=tf, output_at_times=hydrostatic_tank.output_at_times
        )
        return scheme

    def create_particles(self):
        p_arrays = super(EDACHT, self).create_particles()
        self.scheme.setup_properties(p_arrays)
        return p_arrays

    def create_equations(self):
        return self.scheme.get_equations()

    def create_solver(self):
        return self.scheme.get_solver()

if __name__ == '__main__':
    app = EDACHT()
    app.run()
    app.post_process(app.info_filename)

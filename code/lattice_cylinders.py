# PySPH imports
from pysph.examples import lattice_cylinders

# the equations
from edac import EDACScheme

dt = lattice_cylinders.dt
tf = lattice_cylinders.tf
nu = lattice_cylinders.nu
c0 = lattice_cylinders.c0
rho0 = lattice_cylinders.rho0
p0 = lattice_cylinders.p0
fx = lattice_cylinders.fx


class EDACLC(lattice_cylinders.LatticeCylinders):
    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=['solid'], dim=2, c0=c0, nu=nu,
            rho0=rho0, pb=p0, gx=fx, h=lattice_cylinders.h0
        )
        scheme.configure_solver(
            tf=tf, dt=dt, adaptive_timestep=False
        )
        return scheme


if __name__ == '__main__':
    app = EDACLC()
    app.run()
    app.post_process(app.info_filename)

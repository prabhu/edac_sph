"""Poiseuille flow using the EDAC formulation."""

# PySPH imports
from pysph.examples.poiseuille import PoiseuilleFlow

# the eqations
from edac import EDACScheme


class EDACPoiseuilleFlow(PoiseuilleFlow):
    def add_user_options(self, group):
        super(EDACPoiseuilleFlow, self).add_user_options(group)
        group.add_argument(
            "--use-pb", action="store_true", default=False,
            help="Use background pressure for EDAC."
        )

    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=['channel'], dim=2, rho0=self.rho0,
            c0=0.0, nu=self.nu, pb=0.0, gx=None, eps=0.0
        )
        return scheme

    def configure_scheme(self):
        tf = 100.0
        print("dt = %g"%self.dt)
        p0 = self.p0 if self.options.use_pb else 0.0
        self.scheme.configure(c0=self.c0, pb=p0,  gx=self.fx)
        self.scheme.configure_solver(dt=self.dt, tf=tf, pfreq=1000)


if __name__ == '__main__':
    app = EDACPoiseuilleFlow()
    app.run()
    app.post_process(app.info_filename)

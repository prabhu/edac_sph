"""Taylor Green vortex flow"""

# PySPH imports
from pysph.examples import taylor_green

# the eqations
from pysph.sph.equation import Group
from pysph.sph.basic_equations import ContinuityEquation, XSPHCorrection
from pysph.sph.wc.viscosity import LaminarViscosity
from pysph.sph.wc.basic import TaitEOS

from pysph.sph.wc.basic import MomentumEquation as StandardMEquation
from pysph.sph.wc import transport_velocity as TVF

from edac import ComputeAveragePressure, MomentumEquationPressureGradient

from edac import EDACScheme
from pysph.sph.scheme import SchemeChooser


c0 = taylor_green.c0
rho0 = taylor_green.rho0
p0 = taylor_green.p0


class StandardMEWithBasaCorrection(StandardMEquation):

    def loop(self, d_idx, s_idx, d_rho, d_cs,
             d_p, d_au, d_av, d_aw, d_pavg, s_m,
             s_rho, s_cs, s_p, VIJ,
             XIJ, HIJ, R2IJ, RHOIJ1, EPS,
             DWIJ, DT_ADAPT, WIJ, WDP):

        rhoi21 = 1.0/(d_rho[d_idx]*d_rho[d_idx])
        rhoj21 = 1.0/(s_rho[s_idx]*s_rho[s_idx])

        vijdotxij = VIJ[0]*XIJ[0] + VIJ[1]*XIJ[1] + VIJ[2]*XIJ[2]

        piij = 0.0
        if vijdotxij < 0:
            cij = 0.5 * (d_cs[d_idx] + s_cs[s_idx])

            muij = (HIJ * vijdotxij)/(R2IJ + EPS)

            piij = -self.alpha*cij*muij + self.beta*muij*muij
            piij = piij*RHOIJ1

        # compute the CFL time step factor
        _dt_cfl = 0.0
        if R2IJ > 1e-12:
            _dt_cfl = abs( HIJ * vijdotxij/R2IJ ) + self.c0
            DT_ADAPT[0] = max(_dt_cfl, DT_ADAPT[0])

        pavg = d_pavg[d_idx]
        tmpi = (d_p[d_idx] - pavg)*rhoi21
        tmpj = (s_p[s_idx] - pavg)*rhoj21

        fij = WIJ/WDP
        Ri = 0.0; Rj = 0.0

        #tmp = d_p[d_idx] * rhoi21 + s_p[s_idx] * rhoj21
        #tmp = tmpi + tmpj

        # tensile instability correction
        if self.tensile_correction:
            fij = fij*fij
            fij = fij*fij

            if d_p[d_idx] > 0 :
                Ri = 0.01 * tmpi
            else:
                Ri = 0.2*abs( tmpi )

            if s_p[s_idx] > 0:
                Rj = 0.01 * tmpj
            else:
                Rj = 0.2 * abs( tmpj )

        # gradient and correction terms
        tmp = (tmpi + tmpj) + (Ri + Rj)*fij

        d_au[d_idx] += -s_m[s_idx] * (tmp + piij) * DWIJ[0]
        d_av[d_idx] += -s_m[s_idx] * (tmp + piij) * DWIJ[1]
        d_aw[d_idx] += -s_m[s_idx] * (tmp + piij) * DWIJ[2]



class EDACTaylorGreen(taylor_green.TaylorGreen):
    def add_user_options(self, group):
        super(EDACTaylorGreen, self).add_user_options(group)
        group.add_argument(
            "--remesh", action="store", type=float, dest="remesh", default=0,
            help="Remeshing frequency (setting it to zero disables it)."
        )
        group.add_argument(
            "--bql", action="store_true", dest="bql", default=False,
            help="Apply the Basa, Quinlan and Lastiwka correction (non-EDAC)."
        )
        group.add_argument(
            "--no-bql-edac", action="store_false", dest="edac_bql",
            default=True,
            help="Don't apply the Basa, Quinlan and Lastiwka correction "
            "for EDAC."
        )

    def configure_scheme(self):
        pb = self.options.pb_factor*p0
        h0 = self.hdx * self.dx
        if self.options.scheme == 'edac':
            self.scheme.configure(
                nu=self.nu, h=h0, pb=pb, bql=self.options.edac_bql,
                alpha=self.options.alpha
            )
        super(EDACTaylorGreen, self).configure_scheme()

    def create_scheme(self):
        s = super(EDACTaylorGreen, self).create_scheme()
        scheme_dict = s.schemes
        wcsph_scheme = scheme_dict['wcsph']
        wcsph_scheme.configure(alpha=0.5)
        edac_scheme = EDACScheme(
            fluids=['fluid'], solids=[], dim=2, c0=c0, nu=0.0, rho0=rho0,
            pb=p0, eps=0.0, h=0.0
        )
        scheme_dict['edac'] = edac_scheme
        s = SchemeChooser(default='edac', **scheme_dict)
        return s

    def create_particles(self):
        [fluid] = super(EDACTaylorGreen, self).create_particles()
        extra_props = ('pavg', 'nnbr') if self.options.bql else ()
        for prop in extra_props:
            fluid.add_property(prop)

        fluid.add_output_arrays(list(extra_props))
        return [fluid]

    def create_solver(self):
        scheme = self.options.scheme
        if scheme == 'wcsph' or scheme == 'tvf':
            return super(EDACTaylorGreen, self).create_solver()
        else:
            return self.scheme.get_solver()

    def create_equations(self):
        scheme = self.options.scheme
        if self.options.bql and scheme == 'wcsph':
            gamma = self.scheme.scheme.gamma
            equations = [
                Group(equations=[
                        TaitEOS(dest='fluid', sources=None, rho0=rho0,
                                c0=c0, gamma=gamma),
                        ComputeAveragePressure(dest='fluid', sources=['fluid']),
                        ], real=False),

                Group(equations=[
                        ContinuityEquation(dest='fluid',  sources=['fluid',]),

                        StandardMEWithBasaCorrection(
                            dest='fluid', sources=['fluid'], alpha=0.1,
                            beta=0.0, c0=c0,
                            tensile_correction=self.options.tensile_correction
                        ),

                        LaminarViscosity(
                            dest='fluid', sources=['fluid'], nu=self.nu
                        ),

                        XSPHCorrection(dest='fluid', sources=['fluid']),

                        ],),

                ]
        elif self.options.bql and scheme == 'tvf':
            equations = [
                # Summation density along with volume summation for the fluid
                # phase. This is done for all local and remote particles. At
                # the end of this group, the fluid phase has the correct
                # density taking into consideration the fluid and solid
                # particles.
                Group(
                    equations=[
                        TVF.SummationDensity(dest='fluid', sources=['fluid']),
                        ], real=False),

                # Once the fluid density is computed, we can use the EOS to
                # set the fluid pressure. Additionally, the shepard filtered
                # velocity for the fluid phase is determined.
                Group(
                    equations=[
                        TVF.StateEquation(dest='fluid', sources=None,
                                      p0=p0, rho0=rho0, b=1.0),
                        ], real=False),

                Group(equations=[
                    ComputeAveragePressure(dest='fluid', sources=['fluid']),
                    ],
                    real=False
                ),
                # The main accelerations block. The acceleration arrays for
                # the fluid phase are updated in this stage for all local
                # particles.
                Group(
                    equations=[
                        # Pressure gradient terms -- this is from the edac
                        # scheme as it incorporates the BQL correction.
                        MomentumEquationPressureGradient(
                            dest='fluid', sources=['fluid'],
                            pb=p0*self.options.pb_factor
                        ),

                        # fluid viscosity
                        TVF.MomentumEquationViscosity(
                            dest='fluid', sources=['fluid'], nu=self.nu),

                        # Artificial stress for the fluid phase
                        TVF.MomentumEquationArtificialStress(
                            dest='fluid', sources=['fluid']
                        ),

                        ], real=True
                ),
            ]
        elif (scheme in ['wcsph', 'tvf']) and not self.options.bql:
            msg = 'If you want to run the TVF/WCSPH, use the pysph example.'
            raise RuntimeError(msg)
        else:
            equations = self.scheme.get_equations()
        return equations

    def create_tools(self):
        tools = []
        if self.options.remesh > 0:
            from pysph.solver.tools import SimpleRemesher
            remesher = SimpleRemesher(
                self, 'fluid', props=['u', 'v', 'uhat', 'vhat'],
                freq=self.options.remesh
            )
            tools.append(remesher)
        return tools


if __name__ == '__main__':
    app = EDACTaylorGreen()
    app.run()
    app.post_process(app.info_filename)

import numpy as np

from pysph.base.utils import get_particle_array_wcsph
from pysph.base.kernels import QuinticSpline

from pysph.solver.application import Application

from pysph.sph.scheme import WCSPHScheme, AdamiHuAdamsScheme, SchemeChooser
from edac import EDACScheme


class TwoBlocks(Application):

    """Simulates two identical blocks of fluid moving towards each other at
    constant speed.  The fluid is inviscid.  The problem is as discussed in

    Marrone et al., Prediction of energy losses in water impacts using
    incompressible and weakly compressible models, Journal of Fluids and
    Structures, 2015: http://dx.doi.org/10.1016/j.jfluidstructs.2015.01.014

    """
    def initialize(self):
        self.rho = 1.0
        self.hdx = 1.0
        self.U = 1.0

    def add_user_options(self, group):
        super(TwoBlocks, self).add_user_options(group)
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=100,
            help="Number of points along x direction."
        )
        group.add_argument(
            "--mach-number", action="store", type=float, dest="mach",
            default=0.01, help="Use specified Mach number."
        )

    def consume_user_options(self):
        self.nx = self.options.nx
        self.dx = 1.0/self.nx
        self.h = self.dx*self.hdx
        self.c0 = self.U/self.options.mach
        self.dt = 0.25*self.h/(self.c0+self.U)
        self.tf = 0.167
        self.pfreq = 50

    def create_particles(self):
        nx = self.nx
        dx = self.dx
        x1, y1 = np.mgrid[-1:1:nx*2j,0:1:nx*1j]
        x1, y1 = x1.ravel(), y1.ravel() + 0.5*dx
        x2, y2 = x1.copy(), y1 - 1.0 - dx

        x = np.concatenate((x1, x2))
        y = np.concatenate((y1, y2))
        h = np.ones_like(x)*self.h
        m = np.ones_like(x) * dx * dx * self.rho
        rho = np.ones_like(x)*self.rho
        v = np.ones_like(x)
        v[y>=0] = -1.0

        fluid = get_particle_array_wcsph(name='fluid', x=x, y=y, h=h, m=m, v=v, rho=rho)

        self.scheme.setup_properties([fluid])
        if 'V' in fluid.properties:
            fluid.V[:] = 1.0/(dx*dx)
        return [fluid]

    def create_scheme(self):
        # These are defaults, and are overridden by command line args.
        alpha = 0.0
        beta = 0.0
        gamma = 1.0
        h = 1.0/50*self.hdx
        edac = EDACScheme(
            fluids=['fluid'], solids=[], dim=2, c0=0.0,
            rho0=self.rho, pb=0.0, nu=0.0, h=h, eps=0.5,
            alpha=1.0, artificial_alpha=alpha
        )
        wcsph = WCSPHScheme(
            fluids=['fluid'], solids=[], dim=2, c0=0,
            rho0=self.rho, h0=h, hdx=self.hdx, nu=0.0, gamma=gamma,
            alpha=alpha, beta=beta, tensile_correction=False
        )
        aha = AdamiHuAdamsScheme(
            fluids=['fluid'], solids=[], dim=2, rho0=self.rho,
            c0=0.0, h0=h, nu=0.0, alpha=alpha, gamma=gamma
        )
        return SchemeChooser(
            default='edac', wcsph=wcsph, edac=edac, aha=aha
        )

    def configure_scheme(self):
        scheme = self.scheme
        if self.options.scheme == 'edac':
            scheme.configure(
                artificial_alpha=self.options.alpha, c0=self.c0, h=self.h
            )
        else:
            scheme.configure(
                alpha=self.options.alpha, c0=self.c0, h0=self.h
            )
        kernel = QuinticSpline(dim=2)
        self.scheme.configure_solver(
            kernel=kernel, dt=self.dt, tf=self.tf, pfreq=self.pfreq,
            output_at_times=[0.007]
        )

    def post_process(self, info_fname):
        try:
            import matplotlib
            matplotlib.use('Agg')
        except ImportError:
            print("Post processing requires matplotlib.")
            return
        info = self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        import os
        from pysph.solver.utils import load

        t1 = 0.007
        n_t1 = int(round(t1/self.dt)/self.pfreq) + 1
        data = load(self.output_files[n_t1])
        assert abs(data['solver_data']['t'] - t1) < 1e-10
        x1, y1, p1 = self._plot_pressure(
            data, 'p_t007.pdf', 'Ut/L=0.007', vmin=-1.0, vmax=1.0
        )

        data = load(self.output_files[-1])
        t = data['solver_data']['t']
        x2, y2, p2 = self._plot_pressure(
            data, 'p_final.pdf', 'Ut/L=%.3f'%t
        )

        fname = os.path.join(self.output_dir, 'results.npz')
        np.savez(fname, x1=x1, y1=y1, p1=p1, x2=x2, y2=y2, p2=p2, tf=t)

    def _plot_pressure(self, data, fname, title='', **kw):
        import os
        from matplotlib import pyplot as plt
        pa = data['arrays']['fluid']

        factor = self.rho*self.U*self.c0
        plt.scatter(
            pa.x, pa.y, c=pa.p/factor, marker='.',
            s=10, cmap='jet', edgecolors='none', **kw
        )
        plt.axis('tight')
        plt.xlabel('x'); plt.ylabel('y'); plt.title(title)
        plt.colorbar()
        fig = os.path.join(self.output_dir, fname)
        plt.savefig(fig, dpi=300)
        if not fig.endswith('.png'):
            base = os.path.splitext(fig)[0]
            plt.savefig(base + '.png', dpi=150)
        plt.close()
        return pa.x, pa.y, pa.p/factor


if __name__ == '__main__':
    app = TwoBlocks()
    app.run()
    app.post_process(app.info_filename)
